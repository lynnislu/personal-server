mkdir -p ~/workspace/projects
cd ~/workspace/projects
git clone https://gitlab.com/bminusl/personal-server.git
git clone https://gitlab.com/bminusl/bminusl.xyz.git
git clone https://gitlab.com/bminusl/cv.git
git clone https://gitlab.com/bminusl/twtxt.bminusl.xyz.git

cd
git clone https://gitlab.com/bminusl/dotfiles.git
cd dotfiles
./install-dotfiles.sh
